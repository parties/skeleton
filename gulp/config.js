module.exports = (function () {
  return {
    watch: {
      scripts: 'public/js/**/*.js',
      styles: 'styles/**/*.scss'
    },
    paths: {
      scripts: {},
      styles: [
        'styles/sass/site.scss'
      ]
    },
    dest: 'public/dist'
  }
})();
