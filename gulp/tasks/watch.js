var gulp = require('gulp');
var config = require('../config');

gulp.task('watch', function() {
  gulp.watch(config.watch.scripts, ['browserify']);
  gulp.watch(config.watch.styles, ['styles']);
});
