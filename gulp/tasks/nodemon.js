var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('nodemon', function() {
  nodemon({script: 'server.js', ext: 'html js scss', ignore: ['ignore.js']})
    .on('change', ['browserify', 'styles'])
    // .on('restart', function() {
    //   console.log(Date.now() + ' - Server restarted');
    // });
})
