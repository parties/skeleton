(function () {
  'use strict';

  var sass = require('node-sass');
  var express = require('express');
  var app = express();

  app.use(function (req, res, next) {
    console.log('REQ:', req.url);
    next();
  });

  app.use(express.static(__dirname + '/public'));

  var server = app.listen(4000, function () {
    console.log('Listening on port %d', server.address().port);
  });
})();