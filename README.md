## Web App Skeleton Framework
### Eric Ogden

node + express + angular + sass

**Note**: requires the `sass` Ruby gem to be installed

1. `bower install && npm install`
1. `gulp` then `node server.js`