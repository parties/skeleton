'use strict';

var q = require('q');
var angular = require('angular');

angular.module('app', [])

.controller('MainCtrl', [
  '$scope', '$http', '$window', '$q', '$log',
  function ($scope, $http, $window, $q, $log) {
    $scope.title = 'Skeleton';
  }
])
